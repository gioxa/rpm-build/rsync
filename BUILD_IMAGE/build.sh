export target="${CI_PROJECT_DIR}/rootfs"
export BASE="${CI_PROJECT_DIR}/"
export OS_CONFIG=make_os.conf
./make_os
mkdir -pv $target/builds
mkdir -pv /builds/.npm-packages/{bin,share/man,lib/node_modules}
echo "prefix = /builds/.npm-packages" >> $target/builds/.npmrc
chmod -R 770 /builds/.npm-packages
